package br.com.paulo.phonecell.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by paulo on 07/04/16.
 */
public class EstoqueCelular {
    private Map<String,Celular> celulares = new HashMap<>();

    public EstoqueCelular(){
        Celular primeiroCelular = new Celular("Nexus 6","HTC","Smartphone Oficial da Google!", Celular.SistemaOperacional.ANDROID);
        Celular segundoCelular = new Celular("Iphone 6S", "Apple", "O Top de Linha da Apple!", Celular.SistemaOperacional.IOS);
        Celular terceiroCelular = new Celular("Lumia 950 XL","Microsoft","O Aparelho repaginado da Microsoft!", Celular.SistemaOperacional.WINDOWS_PHONE);

        celulares.put(primeiroCelular.getNome(), primeiroCelular);
        celulares.put(segundoCelular.getNome(), segundoCelular);
        celulares.put(terceiroCelular.getNome(), terceiroCelular);
    }

    public Collection<Celular> listarCelular() {
        return new ArrayList<>(this.celulares.values());
    }

    public void adicionarCelular (Celular celular) {
        this.celulares.put(celular.getNome(), celular);
    }

    public Celular recuperarCelularPeloNome(String nome){
        return this.celulares.get(nome);
    }
}
