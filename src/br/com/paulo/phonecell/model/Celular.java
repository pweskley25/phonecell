package br.com.paulo.phonecell.model;

/**
 * Created by paulo on 07/04/16.
 */
public class Celular {
    private String nome;
    private String fabricante;
    private String descricao;
    private SistemaOperacional SistemaOperacional;

    public enum SistemaOperacional {
        ANDROID, WINDOWS_PHONE, IOS;
    }

    public Celular(String nome, String fabricante, String descricao, Celular.SistemaOperacional sistemaOperacional) {
        this.nome = nome;
        this.fabricante = fabricante;
        this.descricao = descricao;
        SistemaOperacional = sistemaOperacional;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public SistemaOperacional getSistemaOperacional() {
        return SistemaOperacional;
    }

    public void setSistemaOperacional(SistemaOperacional SistemaOperacional) {
        this.SistemaOperacional = SistemaOperacional;
    }

    public String toString() {
        return this.getNome() + " - " + this.getDescricao();
    }
}
